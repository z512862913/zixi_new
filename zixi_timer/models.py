# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.
class Student(models.Model):
    student_id = models.CharField(u'学号', max_length=13)
    zixi_time_total = models.PositiveIntegerField(u'总的自习时间', default=0)

    def get_defeat_percentage(self):
        total_student = Student.objects.all().count()
        less_student_count = Student.objects.filter(zixi_time_total__lt=self.zixi_time_total).count()
        percentage = less_student_count * 100 / total_student
        return percentage
