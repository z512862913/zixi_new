# -*- coding: utf-8 -*-
import time
import logging
import re

from django.http import JsonResponse

from zixi_timer.models import *


logger = logging.getLogger('view_logger')


# 取出用户ip
def get_ip(request):
    if 'HTTP_X_FORWARDED_FOR' in request.META:
        ip = request.META['HTTP_X_FORWARDED_FOR']
    else:
        ip = request.META['REMOTE_ADDR']
    return ip


# 接收POST方法发出的学号和本次自习时间，单位为分钟
# 存入数据库的是学号对应的总的自习时间
# 数据只记录早上六点开始到第二天早上两点之间的数据
def rcv_time(request):
    ip = get_ip(request)
    if request.method == 'POST':
        start_time_str = request.POST.get('zixi_start_time')
        if re.search(r'(\d+):(\d+):(\d+)', start_time_str):
            start_time_tuple = start_time_str.split(':')
            student_id = request.POST.get('student_id')
            zixi_time_str = request.POST.get('zixi_time')
            if 2 <= int(start_time_tuple[0]) < 6:
                logger.info('%s\t%s\t%s\t%s\t%s\t%s' %
                            (False, ip, request.method, 'time for sleep', student_id, start_time_str))
                result = {'success': False, 'message': 'it’s time for sleep'}
                return JsonResponse(data=result, status=400)
            else:
                if student_id:
                    if zixi_time_str:
                        try:
                            zixi_time_int = int(zixi_time_str)
                        except ValueError:
                            logger.error('%s\t%s\t%s\t%s\t%s\t%s' %
                                         (False, ip, request.method, 'not a integer', student_id, start_time_str))
                            result = {'success': False, 'message': 'zixi_time must be a integer'}
                            return JsonResponse(result, status=400)
                        if zixi_time_int >= 0:
                            try:
                                student = Student.objects.get(student_id=student_id)
                            except Student.DoesNotExist:
                                student = Student(student_id=student_id)
                            student.zixi_time_total += zixi_time_int
                            student.save()
                            logger.info('%s\t%s\t%s\t%s\t%s\t%s\t%s' %
                                        (True, ip, request.method, 'success',
                                         student_id, zixi_time_int, start_time_str))
                            result = {'success': True, 'message': 'post success'}
                            return JsonResponse(result, status=200)
                        else:
                            logger.error('%s\t%s\t%s\t%s\t%s\t%s\t%s' %
                                         (False, ip, request.method, 'not positive',
                                          student_id, zixi_time_int, start_time_str))
                            result = {'success': False, 'message': 'zixi_time must be a positive integer'}
                            return JsonResponse(result, status=400)
                    else:
                        logger.error('%s\t%s\t%s\t%s\t%s' % (False, ip, request.method, 'need zixi_time', student_id))
                        result = {'success': False, 'message': 'need zixi_time'}
                        return JsonResponse(result, status=400)
                else:
                    logger.error('%s\t%s\t%s\t%s' % (False, ip, request.method, 'need student ID'))
                    result = {'success': False, 'message': 'need student ID'}
                    return JsonResponse(result, status=400)
        else:
            logger.error('%s\t%s\t%s\t%s' % (False, ip, request.method, 'need zixi start time'))
            result = {'success': False, 'message': 'need zixi start time'}
            return JsonResponse(data=result, status=400)
    else:
        logger.error('%s\t%s\t%s\t%s' % (False, ip, request.method, 'method error'))
        result = {'success': False, 'message': 'method %s is not supported' % request.method}
        return JsonResponse(data=result, status=405)


# 接受一个GET方法发出的带有学号的请求
# 取出学号对应的总的自习时间
def get_total_time(request):
    if request.method == 'POST':
        student_id = request.POST.get('student_id')
        if student_id:
            try:
                student = Student.objects.get(student_id=student_id)
            except Student.DoesNotExist:
                result = {'success': True, 'message': 'this student ID do not have any data',
                          'total_time': 0, 'student_id': student_id, 'defeat_percentage': 0}
                return JsonResponse(result, status=200)
            result = {'success': True, 'message': 'get total time success',
                      'total_time': student.zixi_time_total, 'student_id': student_id,
                      'defeat_percentage': student.get_defeat_percentage()}
            return JsonResponse(result, status=200)
        else:
            result = {'success': False, 'message': 'need student ID'}
            return JsonResponse(result, status=400)
    else:
        result = {'success': False, 'message': 'method %s is not supported' % request.method}
        return JsonResponse(data=result, status=405)


def get_user_data(request):
    data = Student.objects.all().order_by('-zixi_time_total')
    student_number = 0
    with open('ranking_list', 'w') as f:
        for stu in data:
            student_number += 1
            f.write(str(stu.student_id)+'\t'+str(stu.zixi_time_total)+'\n')
    result = {'success': True, 'student_number': student_number}
    return JsonResponse(result, status=200)