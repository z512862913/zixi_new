from django.conf.urls import url

urlpatterns = [
    url(r'^rcv_time/$', 'zixi_timer.views.rcv_time'),
    url(r'^get_total_time/$', 'zixi_timer.views.get_total_time'),
    url(r'^get_user_data/$', 'zixi_timer.views.get_user_data'),
]
